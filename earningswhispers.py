from parser import getHTML

class EarningsWhispers:
    def __init__(self):
        self.query = 'https://www.earningswhispers.com/stocks/{}'
    
    def getDateForTicker(self, ticker):
        soup = getHTML(self.query.format(ticker))
        datebox = soup.find('div', {'id':'datebox'})
        days = [div.text for div in datebox if div.text != '']
        return days
