from parser import getHTML

class Zachs:
    def __init__(self):
        self.query = 'https://www.zacks.com/stock/quote/{}'
    
    def getDateForTicker(self, ticker):
        soup = getHTML(self.query.format(ticker))
        return [a.text for a in soup.find('section', {'id':'stock_key_earnings'}).find_all('td')][9]

