from parser import getHTML

class Finviz:
    def __init__(self):
        self.query = 'https://finviz.com/quote.ashx?t={}'

    def getAllData(self, ticker):
        soup = getHTML(self.query.format(ticker))
        x = [a.text for a in soup.find('table', {'class':'snapshot-table2'}).find_all('td')]
        data = dict(zip(x[::2],x[1::2]))
        return data
        
    def getDateForTicker(self, ticker):
        return self.getAllData(ticker)['Earnings']