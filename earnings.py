from earningswhispers import EarningsWhispers
from finviz import Finviz
from zachs import Zachs

tickerList = ['amzn', 'fb', 'goog']
ew = EarningsWhispers()
fv = Finviz()
za = Zachs()

for ticker in tickerList:
    e = ew.getDateForTicker(ticker)
    f = fv.getDateForTicker(ticker)
    z = za.getDateForTicker(ticker)
    print(e, f, z)